#!/usr/bin/python

#
# This walks through the filesystem,
# builds an interactive radial tree w/ d3js from the found folders and ".md" files
# writes an index.html
# renders the .md files to html files
# 
# note: index.html can directly be opened in the browser, no server needed.
#
# usage example:   ./graph-generator/graph-generator.py ../notes ../public
# 									notes is the input dir, public the output dir
#

import os, sys, markdown, shutil
from pathlib import Path

flat = []			# flattened list [[root,name]] will become "nodes" for d3.layout.force
links = []		# [{source:id, target:id}] will become "links" for d3.layout.force
firstParent = ""

def __main__():
	print("")
	print("___________________________________________")
	print("graph-generator for making stuff cool again")
	print("___________________________________________")
	print("")
	if len(sys.argv)>2:
		inputDir = sys.argv[1]
		outputDir = sys.argv[2]
		print("now processing...")
		createOutTree(os.listdir(inputDir), inputDir, 0, 0, outputDir)
		writeIndexHtml(getHtmlCode(flat,links), outputDir)
		print("done.\n")
	else:
		print("usage: graph-generator.py inputDir outputDir")


def createOutTree(node, parent, currentCount, countBefore, outputDir):
	global firstParent

	if(currentCount==0):
		obj = {"name":"ROOT"}
		flat.append(obj)
		currentCount += 1

	if(firstParent == ""):
		firstParent = parent

	if(isinstance(node,list)):
		for child in node:
			full = parent+"/"+child
			isDir = os.path.isdir(full)
			pathOut = full.replace(firstParent, outputDir)

			if(child[-4:]==".png"):
				shutil.copyfile(full, pathOut)
				continue

			if(not isDir and child[-3:]!=".md"):
					continue
			name = child.upper() if isDir else Path(child).stem

			obj = {"source":currentCount, "target":countBefore}
			links.append(obj)

			obj = { "name": name,
							"url": os.path.abspath(pathOut),
							"contentExists":"True" if fileHasContent(full) else "False"}
			flat.append(obj)

			generateHtmlFromMarkdown(full, pathOut)

			if os.path.isfile(full):
				currentCount += 1
			else:
				currentCount = createOutTree(os.listdir(full), full, currentCount+1, currentCount, outputDir)

	return currentCount

# dir and files >50 bytes are considered "having content"
def fileHasContent(filename):
	if os.path.isdir(filename):
		return True
	if os.path.isfile(filename):
		return os.stat(filename).st_size > 50
	return False


# example: 
#
# pathIn 		= ./in/A/a.md
# pathOut 	= ./out
# result: ./out/A/a.md
def generateHtmlFromMarkdown(pathIn, pathOut):
	srcFilename = os.path.basename(pathIn)

	if os.path.isdir(pathIn) and not os.path.exists(pathOut):
		os.makedirs(pathOut)

	if(srcFilename[-3:]==".md" and fileHasContent(pathIn)):
		with open(pathIn, 'r') as fIn:
			with open(pathOut+".html", 'w') as fOut:
				fOut.write(markdown.markdown(fIn.read().replace(".md",".md.html")))

def writeIndexHtml(htmlSourceCode, outputDir):
	with open(outputDir+"/index.html", 'w') as f:
		f.write(htmlSourceCode)


# it's the whole affair with data injected
# https://github.com/d3/d3-3.x-api-reference/blob/master/API-Reference.md#d3layout-layouts
# https://github.com/d3/d3-3.x-api-reference/blob/master/Force-Layout.md#force
def getHtmlCode(nodes, link):
	return f'''<html>
	<head>
		<title>Machine learning topics</title>
		<style>
			body {{ background-color: #1e1e1e; color: #ddd; }}
			h1, h2, p {{ text-align:center; }}
			.text {{ font: small monospace; fill: #eee; cursor: pointer; }}
			.link {{ stroke: #802020; }}
			svg circle {{ stroke:grey; r:5px; fill: #520ecb; }}
		</style>
	</head>

<body>
<h1>Machine learning topics</h1>
<h2>How does it all fit together?</h3>
<input type="checkbox">Also regard markdown links</checkbox>

<script src='https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.17/d3.min.js'></script>
<!-- script>{d3js_3_5_17_min()}</script -->

<script>

var width   = 1300
var height  = 1400

var svg = d3.select("body").append("svg")
	.attr("width", width)
	.attr("height", height)
	.attr("id", "canvas")

const data= {{
	"nodes": {nodes}
	,
	"links": {links}
}}

// effectively de-randomize end position
var n = data.nodes.length;
data.nodes.forEach(function(d, i) {{
	d.x = d.y = width / n * i;
}})

const force = d3.layout.force()
	.size([width,height])
	.nodes(d3.values(data["nodes"]))
	.links(data["links"])
	.on("tick",tick)
	.linkDistance(90)
	.charge(-1900)
	.gravity(0.35)
	.start()

const links = svg.selectAll('.link')
	.data(data["links"])
	.enter().append('line')
	.attr("class","link")

const nodes = d3.select('#canvas').selectAll('svg')
	.data(force.nodes())
	.enter()
	.append('svg')
	.append("circle")
	.attr("cx","200px")
	.attr("cy","200px")

const texts = d3.select('#canvas').selectAll('text')
	.data(force.nodes())
	.enter()
	.append('text')
	.attr("class","text")
	.html( (el)=> el.contentExists==="True" ? el.name : "("+el.name+")" )
	.on("click", (el)=>window.open(el.url+".html","_self","noopener noreferrer"))

function tick(e) {{
	nodes
		.attr("cx", (d) => d.x + 'px')
		.attr("cy", (d) => d.y + 'px')
		.call(force.drag)
	
	links.attr('x1', 	(d) => d.source.x)
		.attr('y1',			(d) => d.source.y)
		.attr('x2', 		(d) => d.target.x)
		.attr('y2',			(d) => d.target.y)

	texts.attr('x', (d) => d.x+10)
				.attr('y', (d) => d.y)
}}
	
</script>

</body>
</html>
'''


def d3js_3_5_17_min():
	return '''
'''


__main__()
