# backpropagation

- BackProp is an algorithm
- in order to understand it, the following topics need to be understood before
  - derivatives
  - chain rule
  - gradient descent

## derivative

    d
    ---- * f(x)  =  df / dx
    dx

??
How does the fct-output change w/ respect to x.
What does the fct look like, that describes the change of x.
??

It's another function which describes the change of f(x) when x changes.

### chain rule

    ( g(f(x)) )' = g'(f(x)) * f'(x)

### implicit differentiation

is also the chain rule.

   d                                    dy
  ---- f(y(x)) = f'(y) * y'(x) = f'(y) ----
   dx                                   dx

so, y'(x) = dy

### implicit differentiation example

    x⁴ + y⁴ = 12
    dy / dx
    find dy with respect to dx

- derive for d / dx
- also derive y, but add "* dy/dx" to the term
- then pull dy/dx to the left side of the eq

### partial derivative

simple case - how does the function change, when x changes

    f(x)=x²
    df/dx(Z)      (Leibnitz notation)

same thing but for multi variable fct (how does f change when just one var changes)

    f(x,y) = x² y + sin(y)
    ∂f/∂x(1,2)

Note: ∂ is a stylised cursive d, meaning "partial".


## gradient descent

[See here](./gradient-descent.md)


# ...

How does backprop know the contribution of each output neuron to the global error?
