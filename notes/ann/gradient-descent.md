# what is gradient descent

- gradient descent is an algorithm for finding a minimum or a maximum of a function.
- For some functions we **know a closed form** - e.g. for a polynomial up to the 4th degree.
  - Finding min/max can be achieved via 2nd derivative.
- For some functions we **don't know a closed form** or possibly there isn't one.
- Finding a max or a min on a function where we don't have a closed form requires an algorithm.
- gradient descent is such an algorithm.

# what is a gradient

- a slope, a steepness.
- it's interesting when it's 0 - meaning a horizontal line

# what does descent mean

- gradient descent is used to find a minimum of a function.
- that min is approached in a stepwise fashion.
- it's a very efficient shortcut to brute-forcing a solution

