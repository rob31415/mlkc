# bias/variance tradeoff

- bias = how bad does it fit to the data (lower is better but could be overfit)
- variance = how similar are the sum-of-squares for test datasets vs training datasets (lower is better, high could be underfit)
  - might fit training set very well but not test set
  - different sum of squares between training and test DS means variability is high
- finding sweet spot: regularization, boosting, bagging
