# bootstrapping

tells us what might happen if experiment is repeated many times over.

- given: numberline with N number
- A: create new numberline
- select N measurements randomly from original numberline
- "sampling with replacement", allow to pick the same number multiple times
- calc the mean of bootstrapped dataset
- keep track of the calculation in a histogram
- repeat from A a bunch of times (eg 1000 times)
- you can calc' sth else too, eg. median, std deviation, etc.

- a 95% confidence interval is an interval which covers 95% of the bootstrapped means (which are available in the histogram)

