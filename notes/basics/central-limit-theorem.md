# normal or bell or gaussian curve or distribution

imagine a graph where x axis = height, y axis = likelihood

std deviation is the width of the curve.
95% of measurements are between -2 and +2 std deviations around the mean.

There many other distribs: uniform distribution (just a line) exponential distribution etc.

why is it found in nature so often? -> "central limit theorem"

# calc std deviation of a sample, variance

input = [10,3,6,1], n=4 (lengh of array)

1. avg = 20/4 = 5
1. distances from avg = [5,-2,1,-4]
1. squared = [25,4,1,16]
1. sum of squared = 46
1. 46 / (n-1) = 46 / 3 = 15.3 = VARIANCE
1. sqrt(VARIANCE) = STD DEVIATION = 3.916
    VARIANCE = STD-DEV ²

std dev tells you how much the data is scattered around the mean.

## central limit theorem

the **means** of any distribution are normally distributed.
sample size must be >=30.

## used for...

- confidence intervals
- t test (is there a diff of means-distribution between two sets of samples ("sample mean"))
- anova test (3 or more sets of samples)

note: in practice, distributions without mean is very seldom.

# std error vs std deviation

you take a bunch of samples from some data
you do this, let's say 5 times
std variation is the variation within one set of samples
you take the mean of each of those 5 sets of samples
you now got 5 means
plot the means on a graph
you take the mean of those means
the "std error" is the std deviation of those means

note: std error is already available from 1 set of measurements
