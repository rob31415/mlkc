# accuracy and error rates

                       PRED
                   +         -

    ACTUAL  -  True Neg  False Pos
            +  Fals Neg  True Pos

- False Pos = type I Error
- False Neg = Type II Error (worse)
- Accuracy = Correct / Total = (TN+TP)/total in %
- Error rate = Incorrect / Totoal = (FP+FN)/total in %
- Note: confusion matrix can be arranged in 4 different ways

# accuracy paradox

- you could end up with a higher AR when completely abandoning one part of a binary decision

# cumlative accuracy profile (CAP) / gain chart

- for comparing models

![](./gain-chart.png)

- red = random
- below red = even worse case
- blue = poor model
- green = theoretically ideal
- don't confuse w/ ROC = Receiver Operating Characteristic

## calc

- area under good model / area under perfect line = AR
- rule of thumb - what's the Y value @ 50% X ?
  - < 60% rubbish
  - 60 to 70  poor
  - 70 to 80  good
  - 80 to 90  very good
  - over 90   too good  
    - one independent variable is post-factum, looking in the future
    - or overfitting

# see also

- [sensitivity and specificity](./sensitivity-specificity.md)
- [ROC and AUC](./roc-auc.md)

