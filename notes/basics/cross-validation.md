# cross validation

- for low data volume
- split dataset in N parts of the same size, called "folds"
- then create multiple models by running different training data on them for each model

for instance, 3 fold:

    model1:  [fold1]  fold2   fold3
    model2:   fold1  [fold2]  fold3
    model3:   fold1   fold2  [fold3]
  
the [] means it's a testing fold, all others are training folds

- then average the total of models 
- cross-validation models are not meant for predictions
- this is only for model validation, not for model building
