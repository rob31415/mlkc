# missing data

replace empty data by the mean of the rest of the rows in the same column.

# splitting in training and testing set

80:20

# feature scaling

See about multicollinearity [here](../supervised/regression/linear/linear-regression.md#when-to-use-it)
