# two ways for feature scaling

## standardisation

Xstd = (x-mean(x)) / std-deviation(x)

## normalisation

Xnorm = (x - min(x) ) / max(x) - min(x)

# dealing with categorical data

## dummy variables

example:

    given: 1 col "state" with 2 states, "new york" or "california"

    solution:
    create one col for each state:


    state         new york  california
    new york        1
    california                1
    california                1
    new york        1
    new york        1
    california                1

don't use california column at all, because no info is lost in just using new york column - in fact this would lead to multicollinearity (see [See here](../../../basics/feature-scaling.md#what-to-do with categorial variables/features?") ).

## encoding categorical data

same as above, but just replace "new york" with 1 and "california" with 2 in the "state" column - no extra columns needed.

See also: [feature scaling](./data-preparation.md)
