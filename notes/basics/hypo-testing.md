# Statistical hypothesis test

Decide whether the data sufficiently support a particular hypothesis.
I.e. if something is statistically relevant or just noise.

reject or "fail to reject" a hypothesis (overfitted data).

# null hypothesis

- claim: there is no coincidence/relationship between two parameters/variables.
- doesn't need data

# alternative hypothesis

- claim: there is a difference

# fisher's exact test

