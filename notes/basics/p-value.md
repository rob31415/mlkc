# p-value (statistical significance)

- probability of an observed (or more extreme) result assuming that the null hypothesis holds.
- used for small datasets in place of bayesian
