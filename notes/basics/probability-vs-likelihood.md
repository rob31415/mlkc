# probability vs likelihood

## probability

pr( weight between 32 and 34 grams | mean=32 and std deviation = 2.5) = 0.3

probability of weighing a mouse between 32 and 34 grams, given mean, std dev.... is 0.3

distribution is the curve, described on the right side and the area described on the left side of the eqation.

probability is area under the curve (eb normal distribution).

## likelihood

assume a mouse has been weighted.

e.g. 32g.

the probability of wheighing a 32g mouse is the y-axis value of the point of the curve.

L(mean=32 and std dev 2.5 | maus weighs 34gram) = 0.042

right side is fixed.
modify shape and location w/ left side.

"likelihood of a distribution given the data"
