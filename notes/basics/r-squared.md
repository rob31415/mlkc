# R squared

    R² = 1 - (SSres/SStot)

- SSres = sum of squares of regression line
- SStot = sum of squares of average line

[See also](../supervised/regression/linear/linear-regression.md)


- 1.0 perfect (suspicious)
- ~0.9 very good
- <0.7 not great
- <0.4 terrible
- <0 model makes no sense for this data

# adjusted R squared

goodness of fit (bigger is better)
