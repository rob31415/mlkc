
See also [sensitivity and specificity](./sensitivity-specificity.md)

# ROC

- for making decisions about thresholds
- given: logistic regression
- question: which threshold is best
- try out different thresholds
  - [confusion matrix](./confusion-matrix.md) for each threshold
  - calc true pos rate and
  - false pos rate = (1-specificity) = false pos / (false pos + true neg)
  - plot on 2d graph: x=false pos rate, y=true pos rate
- so, the ROC graph summarizes all of the confusion matrices that each threshold produced
- note that false pos rate is NOT precision
  - precision = true pos / (true pos + false pos)

# AUC

- "area under curve"
- one value for comparing different ROC curves
- bigger AUC = better
