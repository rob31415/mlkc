# rows

## population vs sample

- population = N = all the data
    - numbers are "parameters"
- sample = n = subset of N
    - numbers are "statistics"
- observation = 1 row

- populations are often hard to obtain in their entirety
- samples should be random and representative

# columns

## feature vs dependent variable

- features = variables = columns of a table
- optional column: dependent variable or result (for predictions in supervised learning)
