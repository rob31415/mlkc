regarding a confusion matrix,

- sensitivity = true pos / (true pos + false neg) = true pos rate
- specificity = true neg / (true neg + false pos)

sens = percentage of correctly identified positives
spec = percentage of correctly identified negatives

higher is better for both.

