# concepts

- States and state space
  - A state space is a set of all possible states in the task. E.g. a maze, encoded as a 2x2 matrix where "1" is a wall.
- Actions and action space
  - actions could be encoded as number (0=go up, 1=right, etc.)
- Trajectories and episodes
  - trajectory example: "for _ in range(3)" to take 3 steps, always progressing from state to next state
  - episode example: a loop "while not done" where done is set when goal is reached
- Rewards and returns
  - return at the beginning is G0=R1+γR2+γ2R3+...+γT−1RT
- Policy
  - could be encoded as [0.5, 0.3, 0.1, 0.1], where action 0 has 50% p to be chosen, action 1 0.3%, etc.

# state space
