# elements of a "control task"

- state(St)
	- all relevant info about the situation of the task environment
- action(At)
	- manipulations an agent can do on itself (?and the evironment?)
- reward(Rt)
	- pos and neg value
- the agent
- the environment
	- all aspects of the task, the agent can't control 100%
