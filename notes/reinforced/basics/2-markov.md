# Markov decision process (MDP)

	(A) discrete-time (B) stochastic (C) control process

- A: time moves fwd in finite intervals
- B: future states depend only partially on actions taken
- C: is based on decision making

bla

	(S,A,R,P)

- S = set of possible states,
- A = set of actions, 
- R = set of rewards, 
- P = probabilities of passing from one state to another when taking each possible action

## important property of "Markov process"

- Process has no memory.
- meaning, next state depends only on current state, not on previous states

	P[St+1|St=st] = P[St+1|St=st,St-1=st-1,...,S0=s0]

## difference to Markov chain

- Decision process adds actions and rewards

## finite vs infinite

- finite: # states, # actions, # rewards are finite
- infinite: one of the above three is infinite
	- example: continuous values e.g. car speed

## episodic vs continuing

episodic ends some time, eg chess game

## detail

![illustration](./mdp.png)

In this process, the agent interacts with the environment.

At the start of the task, the agent observes the initial state of the task and based on it, it takes an action.

This action produces an effect on the environment which modifies its state.

As a result, the agent observes again the new state in which the task finds itself and receives a reward from the environment, which gives feedback on the effect that the previous action had on the environment.

Then, based on the new state, the agent takes another action and the cycle repeats itself until the task ends, either because the agent has achieved the goal of the task or because it has failed for some reason.
