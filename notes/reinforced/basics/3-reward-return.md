# trajectory and episode

sequence of elements generated when agent goes through states

	tau = S0,A0, R1,S1,A1, R2,S2,A2, ...

episode is a trajectory from initial to terminal state (of some given task)

# reward and return

- reward represents task goals
	- Rt = immediate result
- wanted: max(sum of rewards); max an episode's return
	- short term reward can worsen long term results

return: 

	Gt = Rt+1 + Rt+2 + Rt+3 + ...

# discount factor

	G0 = R1 + γR2 + γ²R3 + γ³R4 + ...

- γ is between 0 and 1
- if γ=0, only R1 is relevant
	- an extreme: ony short term reward matters
- γ=1
	- more patience to formulate a long term strategy
- default γ=0.99
- γ measures how far an agent has to look into the future for planning it's actions

goal of control task: **maximise the long-term sum of discounted rewards**

Note the difference between Gt and G0.
