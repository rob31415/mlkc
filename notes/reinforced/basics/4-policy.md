# policy

it's a fuction mapping state to action

	π: S -> A

## two possible expressions

probability of action in a given state

	π(a|s)

action in state s

	π(s)

## stochastic vs deterministic policies

	deterministic: π(s) -> a
	stochastic: π(s) = [p(a1),p(a2),...]

## goal

maximise the sum of discounted rewards.

in other words: find the optimal policy π*

The policy that maximises the sum of discounted rewards in the long run.

# value of a state

different policies lead to different actions and different returns.

		vπ(s) = E[Gt|St=s]

q value of an action in a state is the return Gt, when started in state s, taking action a and interact w/ the environment following policy π until the end of an episode.

	qπ(s,a) = E[Gt|St=s,At=a]

q values simplify search for optimal policy in many algorithms.

# on policy vs off policy
