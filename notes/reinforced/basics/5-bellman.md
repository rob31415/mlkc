# bellman equation for v(s)

	vπ(s) 
	= E [Gt|St=s] 
	= E [Rt+1 + γRt+2 + ...|St=s]
	= E [Rt+1 + γGt+1 + ...|St=s] 
	= SUM(π(a|s)) * SUM(p(s,r|s,a)[r+γvπ(ś)])

probability of taking each action following that policy * expected return from taking that action.
That return is a probability of reaching each possible goal * the reward of reaching that state + discounted value of that state.

Note: this is a recursion between the val. of one state vπ(s) and values of other states.

# bellman equation for q(s,a)

![bellman](./bellman.png)

# bellman optimality equations

![bellmanopt](./bellmanopt.png)
