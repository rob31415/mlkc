# DP

- optimal substructure
  - in our case meaning: π* = π*(s1) + π*(s2) + ...
    - optimal policy for each state = optimal overall policy
    - so, our problem has "optimal substructure"
- overlapping subproblems
  - solutions to subproblems are mutually dependent
    - eg Problema A depends on Problema B and vice versa
  - is this given for our problem?
    - looking at the bellman equations, yes that's the case
- DP also does memoization, meaning already calculated values are stored once and then looked up
- DP turns equation into update rule for v*(s)
- but, it needs to know state transition probabilities p(s',r|s,a) in advance and the rewards
- so, DP needs a perfect model of the environment
  - rubic's cube is such a model, but not a maze problem
- DP solves problems using expected values, not trial and error
