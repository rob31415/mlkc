# policy iteration

- 2 functions: V and π  (value and policy)
- init them w/ random values
- V reflects values of each state w/ current policy
- then change policy to a better one
- repeat both functions alternating until both reach an optimum
- functions collabrorate and compete
- "sweeping the state space"

# policy improvement

- is reward of current policy > reward if action is changed
- q function 

      qπ(s,a) = E[p(s',r|s,a)*[r+γvπ(s')]]

- sum of probabilities of arriving at next state after anaction * obtained reward + discounted value

## policy improvement theorem

      if  qπ(s,π'(s)) > vπ(s)  then   vπ'(s)>=vπ(s)

- if q function of π' (pi prime) is better then current policy, π' is better
