# value iteration

- optimal policy = in each state takes action which maximises the return
- to compute return, must know the optimal value of following states
- estimation by iteration

      π*(s) = arg max sum( p(s',r|s,a)[r+γv*(s')] )

- to find: v* (best value)
- note: the environment has to be deterministic
- create table of estimates
  - initial population w/ 0
  - 1st iteration all will be -1 (except goal, it's 0)
- when changes are smaller than some epsilon, stop iteration
