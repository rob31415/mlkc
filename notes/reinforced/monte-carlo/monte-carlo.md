# monte carlo

- family of methods learning based on samples
- agent goes through an episode w/ a certain policy π
  - takes a "trace"
- compute return from every visited state Gt
- value of state = expected return following a certain policy
- averageing all estimated returns
- same thing w/ q values:
  - Qπ(s,a) = average after a specific action

# ley de los grandes números

 mehr ist besser!

# contrast to DP

- DP sweeps through complete state space, incl. states which do NOT lead to the goal
  - exponential complexity
- monte carlo does not
  - doesn't need a complete model of the environment

