# logistic regression

- while linear regression makes predictions, logistic regression usually determines in/out status - i.e. classification of samples
- continuous and discrete
- function is S shaped
  - tells you probabilities from 0 to 1
  - can be an is used for classification (i.e. >50% likelihood is "yes")
- doesn't have concept of "residual" (like linear regression), uses "maximum likelihood" instead
- can be used to assess what variables are useful for classifying samples (see wald's test) and which are not contributing

# maximum likelihood

- how to find the best S shaped curve given some datapoints?
- try a few and calc their likelihood
  - all probabilities multiplied
- best curve is the one with the max likelihood

![](./max-likelihood.png)

# wald's test

