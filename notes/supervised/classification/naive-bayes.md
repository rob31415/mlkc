# naive bayes classification

    posterior = (prior * likelyhood) / normalizing constant
    P(H|D)    = ( P(H)      P(D|H) ) / P(D)

D = data
H = hypothesis

Example

![](./naive-bayes.png)

grey = new point (hypothesis)


- P(H) = red points/total = 10/30
- P(D) = pts similar to new point / total points = 4/30
- P(D|H) = red points in circle / all red points = 3/10
- P(H|D) = P(walking|D) = (10/30 * 3/10) / (4/30) = 0.75

- do the same for P(driving|D)

compare P-walking with P-driving, probability to belong to walking is bigger than the other.

# multinomial naive bayes classifier

- "naive" because ignores order of words (bag of words)
  - eg score("Dear friend") == score("friend Dear")
- has a high bias (ignores order) and low variance (works good in practice)

## how it works

- make a histogram how often each word occurs in a body of text (eg emails)
- calc p(some-word|Normal-messages)
- make one histogram for good messages, and one for spam messages
- these are likelihoods (because discrete, not continuous)
- decide if a new message is normal or spam
  - initial guess ("prior probability") p(N)
  - p(N) * p(Dear|N) * p (friend|N)
  - result is the score if it's a normal message
  - do the same things for bad messages
  - result is the score if it's spam
  - the higher score wins!

## fails

- if a word doesn't exist in the bad messages, it's prob is 0 and therefore misclassification is possible
- remedy: use alpha (just a value that's added to avoid 0; eg 1)

# gaussian naive bayes classification

- draw the gaussian distribution for each parameter
- similar to multinomial, but use likelihood (y coordinate to a corresponding x value, mapped via gaussian distribution)
- eg. p(Loves) * L(popcorn=20|Loves) * L(soda=500|Loves) * L(candy=25|Loves)
- when really small numbers occur, take **log** of everything **to avoid underflow**
- use cross validation to find which parameter to use to make the best possible classification

note: "fold change" is "x fache veränderung"; log2(0) = -inf


