# support vector machines

## maximum margin classifier

margin = distance between an observation and a threshold

    |----x-xx-x-x--x-------y--y-yy-y-y---|
                       ^
                       |
                max margin

max margin is furthest away from last x and first y.

## soft margin

allows outliers.

= support vector classifier, which can be one of

"flat affine N-dimensional subspace"
n = 0 -> point
n = 1 -> line
n = 2 -> plane
n >2  -> hyperplane (aka flat affine subspace)


cross validation can show, if allowing misclassifications might be better in the long run.

## SVM

1. move data into higher dimension 
  - eg for 1D data, add y axis and square each datapoint
2. find a support vector classifier in the higher dimension to separate data in 2 groups

## kernel function

why square? why not cubed or some log?

kernel function raises d step by step and finds a good SVC

polynomial kernel: (a X b + r) ^ d

a,b obervations
r coefficient
d degree of polynomial

transform to dot-prod: (a,a²,1/2) * (b,b², 1/2)

## radial kernel (RBF)

calc the relationship between two observations in infinite dimensions.

behaves like a "weighted nearest neighbor" model.

Taylor series expansion...

## other kernel types

- sigmoid (mostly for NNs)
- custom (domain knowledge)

## kernel trick

don't transform actual data in higher dim, just calc the relationship

## non-linear kernel SVR

see [here](../regression/support-vector-regression.md#non-linear)