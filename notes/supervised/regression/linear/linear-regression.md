# overview

    linear           y = b0 + b1 x1
    multiple linear  y = b0 + b1 x1 + b2 x2  ...
    polynomial       y = b0 + b1 x  + b2 x²  ...

b0 = y intercept
b = slope coefficients
x = independent variables

# linear regression (simple)

- linear regression
  - continuous predictions
  - least squares (sum of squared residuals)
    - find the rotation w/ the least sum of squared residuals
    - y = yIntercept + slope*x
  - SSR/SSE/SST, R squared = SSR/(SSR+SSE), DF
    - SS-regression, SS-error, SS-total
    - how much of the variation is explained by one or many parameters
      - eg. variation in mouse size explained by weight / variation in mouse size without taking weight into account
    - variation around the mean vs variation around the fit
    - r² = SS(mean) - SS(fit) / SS(mean)
    - r² is always getting better when adding more parameters, even if parameters make no sense
  - degrees of freedom
    - DOF = n - k - 1
      - n = #ovservations, k = # x-variables/observations
      - more k, less DOF, more possible error
    - **adjusted r² = 1 - [(1-R²)((n-1)/(n-k-1))]**
      - takes useless parameters into account
  - p value
    - the more silly parameters are added, the more randomness decreases SS(fit) and makes r² better
    - p value tells if r² is statistically significant
    - F = ( [ SSmean - SSfit / (Pfit - Pmean) ] / [ SSfit / (n - Pfit) ] )
      - variance: p-related numbers are the DOF and turn SS to variances
    - p is between 0 and 1; good=0.05
      - getting a small p value when there is actually no difference, is "false positive"
    - p to reject "null hypothesis"
    - small p doesn't imply large effect size
    - calculating
      - one sided: rarely used
      - two sided:
      
# multiple linear regression

  - src:
    - https://www.youtube.com/watch?v=nk2CQITm_eo
    - https://www.youtube.com/watch?v=eYTumjgE2IY
    - https://en.wikipedia.org/wiki/Polynomial_regression

## what is it?

imagine a table with 1 dependent variable and n features.
finding a model that contains the correlation bewteen every feature to the dependant.

## when to use it?

anscombe's quartet: dataset with nearly identical descriptive properties, but vastly different graphs.

![](./anscombe_quartet.png)

- linearity - there is a clear linear relationship between X and Y
- no homoscedasticity (equal variance) - variance depends on independent variable, graphically this results in a cone-like shape
- no multivariate normality (normality of error distribution) - just one error distribution, not more 
  - ![](./multivariate-normality.png)
- no autocorrelation (independence) - rows have to be independent, otherwise there's a pattern in the plot (like zigzag etc.)
- lack of multicollinearity - predictors are not correlated with each other
- outlier check - outlier affects the line we get, consider removing outliers before regression

## categorical features

what to do with categorial variables/features?
See [here](../../../basics/feature-scaling.md#dummy-variables)


# polynomial linear regression

is called linear, because it refers to the coefficients which we want to find, not to x.
