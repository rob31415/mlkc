# regularisation

limiting an algorithms parameter space is called regularisation.

# ridge (L2) regression

- usually, least squares minimizes the sum of squared residuals (see also [here](./linear-regression.md#linear-regression-simple))
- L2 minimizes: sum of squared residuals + lambda * slope²
- makes fit a little worse but is supposed to lower variance
- find best lambda via cross validation (best lambda is where variance is lowest)
- works for continuous and discrete values
- used with small sample sizes or when there are more parameters than data-points

# lasso(L1) regression

- very similar to L2 but uses the absolute of the slope: lambda * |slope|
- can shrink the slope to 0 (ridge can shrink it to close to 0, but never 0 itself)
