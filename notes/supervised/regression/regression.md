# polynomial regression

see [here](./linear/linear-regression.md)

# support vector regression

see [here](./support-vector-regression.md)

# decision tree regression

see [here](../../basics/trees/regression-tree.md)


# random forest regression

see [here](../../ensemble/random-forests.md)
