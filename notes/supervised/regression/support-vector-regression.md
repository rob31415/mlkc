# linear

![](./svr.png)

It's a tube w/ an epsilon instead of just a line.
datapoints outside the tube are the support vectors.

# non linear

![](./non-lin-svr.png)

Instead of a tube, it's now a hyperplane + another hyperplane above + another below.

Projected back to 2d the "tube" is now the yellow area.