# classification tree

- can have numeric w/ boolean data, datatypes can be mixed
- can ask for the same data (e.g. time of exercise) multiple times but w/ differen numeric thresholds
- same leave can occur multiple times

## building a classification tree from a table

Assume a table w/ 4 cols, with 3 features and the last col is the fact.

    Loves Popcorn | Loves Soda | Age | Loves "Cool as ice" movie
      yes               yes       14            yes
      ...

- start with one column, eg popcorn and make a tree

      popcorn ---- true --->  loves movie  yes:1  no:3
               \-- false -->  loves movie  yes:2  no:1

- do the same for other columns
  - leaves that have a mixture of yes no are called "impure"
  - leaves having only yes or no are called "pure"

- quantify impurity of leaves
  - e.g. "Gini impurity", "Entropy", "information gain", etc.
  - gini for a leaf = 1 - p(yes)² - p(no)²
    - p(yes) = #yes / (#yes + #no)
    - p(no) similar but w/ #no in denominator
    - total Gini = weighted avg of leave's impurities
      - weight = (#yes-in-node + #no-in-node) / #total
    - gini impurity of numeric data
      - < is true, > is false
- keep going...
  - put the feature w/ the lowest impurity as the root of the tree
  - if branches are impure, split by next feature, etc.
  - assign output values for each leaf
    - category w/ the most values
    - multiple leaves can have the same value
- now you can run new data down the tree to make predictions
