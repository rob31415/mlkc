# random forests

- create bootstrapped dataset, which is a subset of the original dataset - by choosing K random datapoints
- build a decision tree
- repeat the above N times to get N decision trees, for instance [a regression tree](../regression-tree.md)
- predict using the average of each N decision tree's prediction
- it's a bit like like crowdsourcing, i.e. the prediction of avg(many) is better than that of one expert
  - majority vote
