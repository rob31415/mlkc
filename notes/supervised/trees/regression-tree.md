# regression tree

- each leave is a numeric value
- like a bunch of if/else's or piecewise math. equations
- can easily accomodate many predictors (parameters)
- good for higher dimensional data

## building a regression tree from a table

    dosage | age | sex | etc.. | drug effectiveness
       10     25    F     ...      98
       ...

- start w/ 1 parameter
- take avg of that parameter
- leave asks >avg ?
- calc residuals to quantify quality of prediction
- (pred - observe)² for every point
- we now have added squared residuals
- plot sum and squared residuals on graph (x=dosage threshole, y=sum of sq res)
- find the threshold that gives smallest sum of squared resudials
- do all the above for all other predictors
- compare SSRs from all predictors and pick the lowest as root of the tree

avoid overfitting

- eg. split observations in a node only when more than X obs' (X typically 20)

## building a regression tree from a scatter plot

![plot](./regression-tree-scatterplot.png)

- there are 2 idependent variables (x1 and x2 axis)
- and 1 dependent variable in Y axis (for predictions)
- an algorithm determines where to split
    - uses "information entropy" to do that - means: is a split increasing the amount of information?
    - stops when below certain amount of points OR can't add any more information.
- create binary yes/no tree w/ some terminal leafes from splits
- Y = averages of all points in terminal a leaf.

![plot](./regression-tree.png)


## pruning regression tree 

- cost complexity pruning aka weakest link pruning
