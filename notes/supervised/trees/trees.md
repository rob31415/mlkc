# trees

- **decision tree**
  - convention: left branch TRUE, right branch FALSE
- CART (classification and regression trees)
  - **classification tree** (discrete values) is a decision tree that classifies into categories
  - **regression tree** (continuous values) is when a decision tree predicts numeric values

- see also [Random forests](./ensemble/random-forests.md)
