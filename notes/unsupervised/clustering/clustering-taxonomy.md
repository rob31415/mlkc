# taxonomy of clustering algos

                      flat        hierarchical

    centroid/         k-means     Ward
    parametric        GMM         Complete-linkage

    density/          DBSCAN      HDBSCAN
    non-parametric    Mean shift


# k-means

[see here](./k-means.md)

# hierarchical clustering (Ward)

- similar result as k-means
- agglomerative (bottom up) vs divisive (top down)

steps for agglomerative:

1. make each pt a cluster
2. take two closest pts and make then 1 cluster
3. take two **closest clusters** and merge them
4. repeat 3 until 1 cluster left

## options how to calc distance of clusters

closest points, furtherst points, average distance, dist. between centroids

## dendrogram

![](./dendrogram.png)

note: y axis shows the dis-similarity of clusters

easy to see how many clusters there are at a certain threshold.

### optimal number of clusters

- look for the highest vertical distance that doesn't cross any horizontal lines

# HDBSCAN


# OPTICS

