# dbscan

"density based spatial clustering of applications with noise"

1. user selects a radius ("resolution")
  - acting as a defined vicinity for a point
2. for each point, calc how many points are within it's vicinity
3. user defines # points that have to be within a points vicinity to qualify it as a "core point"
4. randomly pick a core point and assign it to a cluster
5. all points within it's vicinity also go into the cluster, extending it
6. stop at non-core points - adding them to the cluster but not extending it anymore
7. repeat from 4 for remaining points
8. single points that constitute their own cluster constitute outliers
