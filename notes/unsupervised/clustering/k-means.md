# k-means

it's an algorithm:

1. start w/ k clusters (k=6 for example; user selects this)
2. select k points (don't know if arbitrary points in space or from samples)
  - they're called "centroids"
3. assign each sample to it's closest centroid
  - via Squared Euclidean distance which works for any dimension >1
  - meaning this algo works in 2D and higher dimensions
4. calculate new centroids for each cluster based on it's members
5. repeat from 3 until cluster doesn't change anymore (ie no pts added or removed anywhere)
6. calculate mean of each cluster
  - sum(squared distances of each point to the centroid)
  - store it in a map {k, mean}
7. change k and start at 1
8. if you plot the map (x=k, y=mean) there's a bend in the line - a so called "elbow"
  - that's the optimal number for k

Note: problems w/ concave/weirdly cluster shapes. for low sample sizes and low dimensions, to halfway visualize in your mind for selecting params.

## random initialization trap

- can end up with a totally different result because initial centroids were different (random)

kmeans++:

1. choose 1st centroid randomly amongst datapoints
2. for remaining points, compute distance D to the nearest centroid
3. choose next centroid, weightet by D²
