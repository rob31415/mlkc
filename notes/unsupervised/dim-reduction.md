# dimensionality reduction

- project from higher space onto a hyperplane in lower space
- The Swiss Roll dataset contains 1600 observed datapoints in 3 dimensions.
  - The dataset is often used as a test case for dimensionality reduction techniques, such as autoencoders and t-SNE.

**is dim reduction clustering?**

Dimensionality reduction aims to reduce the number of features in a dataset while preserving the underlying structure, whereas clustering aims to group similar data points into clusters.

**two ways**

- Selecting from the existing features (feature selection)
- Extracting new features by combining the existing features (feature extraction)

## Principal Component Analysis (PCA)

- finding similarities between features/dimensions/table-columns
- imagine a table w/ n cols
  - compare each col w/ every other col and make a chart for each (ending up in lots of charts)
  - make an n-dimensional chart (with an axis for each col)
  - both options are unpractical at best and impossible at worst
- solution: PCA Plot converts the correlations (or lack thereof) of all table cells on a 2D graph
- PC1 (x axis) has highter importance than PC2 (y axis)
- can be used to find which variable is most valuable for clustering
- so, it tells us, which two columns correlate the most

### how does it work?

There are two ways to do PCA: 
1) The old way, which applies eigen-decomposition to the covariance matrix and 
2) The new way, which applies singular value decomposition to the raw data. This is preferred because, from a computational stand point, it is more stable.

Step 1: Standardization
Step 2: Covariance Matrix
is a square matrix that describes the covariance between each pair of features.
Step 3: Eigenvalue Decomposition
The covariance matrix is decomposed into its eigenvalues and eigenvectors.
Eigenvalues represent the amount of variance explained by each principal component, while eigenvectors represent the direction of the principal components.
Step 4: Principal Components
The eigenvectors are used to create the principal components, which are new features that are orthogonal to each other.
The principal components are ordered by their corresponding eigenvalues, with the first component explaining the most variance, the second component explaining the second most variance, and so on.
Step 5: Dimensionality Reduction
The original dataset is projected onto the new coordinate system defined by the principal components.
The number of principal components retained can be chosen based on the amount of variance explained by each component or the complexity of the model.

- find the maximum sum of squared distances
  -  why not minimum? 
- linear combination of 

- maximise or minimise?

- eigenvector, eigenvalue?


NOTE 4: A lot of people ask how fitting this line is different from Linear Regression. In Linear Regression we are trying to maintain a relationship between a value on the x-axis, and the value it would predict on the y-axis. In other words, the x-axis is used to predict values on the y-axis. This is why we use the vertical distance to measure error - because that tells us how far off our prediction is for the true value. In PCA, no such relationship exists, so we minimize the perpendicular distances between the data and the line.


## t-SNE ("tee snee")

## heatmaps

## Multi-Dimensional scaling (MDS)

